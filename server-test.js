import express from "express";
import bodyParser from "body-parser";
import viewEngine from "./config/viewEngine";
import initWebRoutes from "./routes/web";
require('dotenv').config();

let app = express()
let router = express.Router()

//Config app
app.use(express.json())
app.use(express.urlencoded({ extended: true }))


/**View engine.js */
app.use(express.static("./public"))
// app.set("view engine", "ejs")
app.set("view engine", "pug")
app.set("views", "./views")

/**Web.js */
router.get('/', (req, res) => {
    res.send('Hello World with Daniel.')
})
app.use("/", router)

//App
let port = process.env.port || 6969

app.listen(port, () => {
    console.log("Backend Nodejs is running on the port: ", port)
})

