import db from "../models/index";
import CRUDService from "../services/CRUDService";

let getHomePage = async (req, res) => {

    try {
        let data = await db.User.findAll()
        return res.render('homepage.pug', {
            data: JSON.stringify(data)
        })

    } catch (error) {
        console.log(error)
    }

}

let getCRUD = (req, res) => {
    return res.render('crud.pug')
}

let postCRUD = async (req, res) => {
    let message = await CRUDService.createNewUser(req.body)
    console.log(message)
    return res.send('User Created')
}

let displayGetCRUD = async (req, res) => {
    let data = await CRUDService.getAllUser()
    // console.log(data)
    return res.render('displayCRUD', {
        dataTable: data
    })
}

let getEditCRUD = async (req, res) => {
    let userId = req.query.id
    if (userId) {
        let userData = await CRUDService.getUserInfoById(userId)
        console.log(userData)
        return res.render('editCRUD', {
            user: userData

        })
    }
    else {
        return res.send("User ID Not Found")
    }
}

let putCRUD = async (req, res) => {
    let data = req.body
    let allUsers = await CRUDService.updateUserData(data)
    return res.render('displayCRUD', {
        dataTable: allUsers
    })

}

let deleteCRUD = async (req, res) => {

    let userId = req.query.id
    if (userId) {
        let allUsers = await CRUDService.deleteUserData(userId)
        return res.render('displayCRUD', {
            dataTable: allUsers
        })
    } else {
        return res.send("User ID Not Found")
    }
}

module.exports = {
    getHomePage: getHomePage,
    getCRUD: getCRUD,
    postCRUD: postCRUD,
    displayGetCRUD: displayGetCRUD,
    getEditCRUD: getEditCRUD,
    putCRUD: putCRUD,
    deleteCRUD: deleteCRUD
}