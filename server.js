import express from "express";
import bodyParser from "body-parser";
import viewEngine from "./config/viewEngine";
import initWebRoutes from "./routes/web";
import connectDB from "./config/connectDB"

require('dotenv').config();

let app = express()

//Config app
app.use(express.json())
app.use(express.urlencoded({ extended: true }))

viewEngine(app)
initWebRoutes(app)

connectDB()

//App
let port = process.env.port || 6969

app.listen(port, () => {
    console.log("Backend Nodejs is running on the port: ", port)
})

