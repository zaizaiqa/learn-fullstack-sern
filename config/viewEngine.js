import express from "express";

let configViewEngine = (app) => {
    app.use(express.static("./public"))
    // app.set("view engine", "ejs")
    app.set("view engine", "pug")
    app.set("views", "./views")
}

module.exports = configViewEngine